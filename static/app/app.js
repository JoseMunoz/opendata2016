var app = angular.module('opendata2016', ['ui.router','leaflet-directive']);


app.config(function ($stateProvider, $urlRouterProvider) {
    console.log("### StateProvider")
    $stateProvider.state('main', {
        url: "/",
        templateUrl: "views/prueba.html",
        controller: 'PruebaController'
    }).state('prueba', {
        url: "/prueba",
        templateUrl: "views/prueba.html",
        controller: 'PruebaController'
    })

    $urlRouterProvider.otherwise('/');
});

app.controller('MainController', ['$scope', '$state', '$rootScope', function ($scope, $state, $rootScope) {
    console.log("### MainController")

}]);

app.controller('PruebaController', ['$scope', '$state', '$rootScope', 'SPARQLService', function ($scope, $state, $rootScope, SPARQLService) {
    console.log("### PruebaController")
    $scope.prueba = "pruebaAngular"

    angular.extend($scope, {
        Caceres: {
            lat: 39.4753,
            lng: -6.3724,
            zoom: 12
        },
        layers: {
            baselayers: {
                mapa: {
                    name: "Mapa IDE del SIG",
                    type: "wms",
                    visible: true,
                    url: "http://ide.caceres.es/geoserver/callejero/wms",
                    layerOptions: {
                        layers: "Callejero_Caceres",
                        format: "image/png",
                        transparent: true,
                        attribution: "© Instituto Geografico Nacional de España"
                    }
                },
                dev: {
                    name: "Dev",
                    type: "wms",
                    visible: true,
                    url: "",
                    layerOptions: {
                        layers: "Callejero_Caceres",
                        format: "image/png",
                        transparent: true,
                        attribution: "© Instituto Geografico Nacional de España"
                    }
                }
                
            }
        },
        markers: {
            mainMarker: {
                lat: 39.4753,
                lng: -6.3724,
                focus: true,
                message: "¡Hola! Estás aquí",
                draggable: true,
                icon: {
                        iconUrl: 'img/girl.png',
                        //shadowUrl: 'img/leaf-shadow.png',
                        iconSize:     [50, 50], // size of the icon
                        iconAnchor:   [32, 32], // point of the icon which will correspond to marker's location
                        popupAnchor:  [-6, -30] // point from which the popup should open relative to the iconAnchor
                    }
            }
        }
    });

    SPARQLService.bares_copas()
        .then(function (datos) {
            console.log(datos)
            var markers = {}

            for(var i in datos.results){
                var lat = parseFloat(datos.results[i].geo_lat)
                var lng = parseFloat(datos.results[i].geo_long)
                if (isNaN(lat)) lat = 1.0
                if (isNaN(lng)) lng = 1.0
                console.log(lat + " " + lng)
                markers["Bares_de_Copas"+i] = {
                    lat: lat,
                    lng: lng,
                    message: datos.results[i].rdfs_label,
                    icon: {
                        iconUrl: 'img/copas.png',
                        //shadowUrl: 'img/leaf-shadow.png',

                        iconSize:     [38, 65], // size of the icon
                        shadowSize:   [50, 64], // size of the shadow
                        iconAnchor:   [22, 64], // point of the icon which will correspond to marker's location
                        shadowAnchor: [4, 62],  // the same for the shadow
                        popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                    }
                }
            }
            $scope.addMarkers(markers)

        }).catch(function (err) { console.log("Error: " + err)})

        SPARQLService.cafes_y_bares()
        .then(function (datos) {
            console.log(datos);
            var markers = {};

            for (i in datos.results){
                var lat = parseFloat(datos.results[i].geo_lat)
                var lng = parseFloat(datos.results[i].geo_long)
                if (isNaN(lat)) lat = 1.0
                if (isNaN(lng)) lng = 1.0
                console.log(lat + " " + lng)
                markers["cafes_y_bares"+i] = {
                    lat: lat,
                    lng: lng,
                    message: datos.results[i].rdfs_label,
                    icon: {
                        iconUrl: 'img/cafe.png',
                        //shadowUrl: 'img/leaf-shadow.png',

                        iconSize:     [30, 30], // size of the icon
                        shadowSize:   [50, 64], // size of the shadow
                        iconAnchor:   [30, 30], // point of the icon which will correspond to marker's location
                        shadowAnchor: [4, 62],  // the same for the shadow
                        popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                    }
                }
            }
            $scope.addMarkers(markers)

        }).catch(function (err) { console.log("Error: " + err)})

    SPARQLService.restaurantes()
        .then(function (datos) {
            console.log(datos);
            var markers = {};

            for (i in datos.results){
                var lat = parseFloat(datos.results[i].geo_lat)
                var lng = parseFloat(datos.results[i].geo_long)
                if (isNaN(lat)) lat = 1.0
                if (isNaN(lng)) lng = 1.0
                console.log(lat + " " + lng)
                markers["restaurantes"+i] = {
                    lat: lat,
                    lng: lng,
                    message: datos.results[i].rdfs_label,
                    icon: {
                        iconUrl: 'img/restaurante23.png',
                        //shadowUrl: 'img/leaf-shadow.png',

                        iconSize:     [45, 45], // size of the icon
                        shadowSize:   [50, 64], // size of the shadow
                        iconAnchor:   [45, 45], // point of the icon which will correspond to marker's location
                        shadowAnchor: [4, 62],  // the same for the shadow
                        popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                    }
                }
            }
            $scope.addMarkers(markers)
            console.log("restaurantes procesados")
            console.log(markers)

        }).catch(function (err) { console.log("Error: " + err)})

    /*SPARQLService.prueba()
        .then(function (datos) {
            console.log(datos)
        }).catch(function (err) {});
    SPARQLService.prueba2()
        .then(function (datos) {
            console.log(datos)
        }).catch(function (err) {});*/

    $scope.addMarkers = function(nuevos) {
        // calcular markers
        //for(var k in nuevos) $scope.markers[k]=nuevos[k];
        $scope.$apply(function () {
           angular.extend($scope.markers, nuevos);
        });

    };

    $scope.removeMarkers = function() {
        $scope.markers = {};
    }
    
}]);


var obtenerMarkersDeDatos = function(datos, nombre, icono, inconsize, inconAnchor) {
    var markers = {};

    for (i in datos.results){
        markers[nombre+i] = {
            lat: parseFloat(datos.results[i].geo_lat),
            lng: parseFloat(datos.results[i].geo_long),
            message: datos.results[i].rdfs_label,
            icon: {
                iconUrl: icono,
                //shadowUrl: 'img/leaf-shadow.png',

                iconSize:     inconsize, // size of the icon
                shadowSize:   [50, 64], // size of the shadow
                iconAnchor:   inconAnchor, // point of the icon which will correspond to marker's location
                shadowAnchor: [4, 62],  // the same for the shadow
                popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
            }
        }
    }
    return markers;
}