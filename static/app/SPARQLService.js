var app = angular.module('opendata2016')

    .factory('SPARQLService', ['$http', function ($http) {

        var server = 'http://opendata.caceres.es/sparql';
        var config = {
            method: 'POST',
            url: server,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            }
        };

        var customPromise = function(url, query){
                config.data = {
                    "default-graph-uri": url,
                    query: query,
                    format: "application/sparql-results+json",
                    timeout: 0,
                    debug: "on"
                };
                const promise = new Promise(function (resolve, reject) {
                    $http(config).then(function (datos) {
                        resolve(limpiaResultado(datos))
                    }, function (err) {
                        reject(new Error('Error al recibir el dato de '+url))
                    });
                });
                return promise
        }

        /********************************* CONSULTAS ***********************************/
        /****************** ¿Cuantos puntos de recogida de basura hay?******************/
        return {
            prueba: function () {
                var query = "select COUNT (?punto_recogida) as ?Numero_puntos_recogida " +
                            " where{ " +
                            " ?punto_recogida a om:PuntoRecogida. }";
                var url = "http://opendata.caceres.es/recurso/medio-ambiente/residuos-urbanos/PuntoRecogida/";

                return customPromise(url, query);
            },
            /******************* Etiqueta + numero de contenedores *********************/
            prueba2: function () {
                var query = "select ?rdfs_label ?om_numeroContenedores " +
                        "where{ " +
                        "?uri a om:PuntoRecogida. " +
                        "?uri rdfs:label ?rdfs_label. " +
                        "?uri om:numeroContenedores ?om_numeroContenedores. " +
                        "}";
                var url = "http://opendata.caceres.es/recurso/medio-ambiente/residuos-urbanos/PuntoRecogida/";
                return customPromise(url, query);
            },

            cafes_y_bares: function () {
               var query =  "SELECT ?geo_long ?geo_lat ?schema_email ?schema_telephone ?rdfs_label ?om_capacidadPersonas ?schema_address_streetAddress ?schema_address_addressLocality ?schema_address_addressCountry ?schema_address_postalCode  WHERE { \
                            ?uri a om:CafeBar. \
                            OPTIONAL  {?uri geo:long ?geo_long. } \
                            OPTIONAL  {?uri geo:lat ?geo_lat. } \
                            OPTIONAL  {?uri schema:email ?schema_email. } \
                            OPTIONAL  {?uri schema:telephone ?schema_telephone. } \
                            OPTIONAL  {?uri rdfs:label ?rdfs_label. } \
                            OPTIONAL  {?uri om:capacidadPersonas ?om_capacidadPersonas. } \
                            OPTIONAL  {?uri schema:address ?schema_address. ?schema_address schema:streetAddress ?schema_address_streetAddress. } \
                            OPTIONAL  {?uri schema:address ?schema_address. ?schema_address schema:addressLocality ?schema_address_addressLocality. } \
                            OPTIONAL  {?uri schema:address ?schema_address. ?schema_address schema:addressCountry ?schema_address_addressCountry. } \
                            OPTIONAL  {?uri schema:address ?schema_address. ?schema_address schema:postalCode ?schema_address_postalCode. }}"
                            
                var url = ""

                return customPromise(url, query);
            },

            bares_copas: function () {
                var query = "SELECT ?geo_long ?schema_url ?geo_lat ?schema_email ?schema_telephone ?rdfs_label ?om_capacidadPersonas ?om_sirveComida ?schema_address_streetAddress ?schema_address_addressLocality ?schema_address_addressCountry ?schema_address_postalCode  WHERE {  \
                            ?uri a om:BarCopas.  \
                            OPTIONAL  {?uri geo:long ?geo_long. } \
                            OPTIONAL  {?uri schema:url ?schema_url. } \
                            OPTIONAL  {?uri geo:lat ?geo_lat. } \
                            OPTIONAL  {?uri schema:email ?schema_email. } \
                            OPTIONAL  {?uri schema:telephone ?schema_telephone. } \
                            OPTIONAL  {?uri rdfs:label ?rdfs_label. } \
                            OPTIONAL  {?uri om:capacidadPersonas ?om_capacidadPersonas. } \
                            OPTIONAL  {?uri om:sirveComida ?om_sirveComida. } \
                            OPTIONAL {?uri schema:address ?schema_address. ?schema_address schema:streetAddress ?schema_address_streetAddress. } \
                            OPTIONAL {?uri schema:address ?schema_address. ?schema_address schema:addressLocality ?schema_address_addressLocality. } \
                            OPTIONAL {?uri schema:address ?schema_address. ?schema_address schema:addressCountry ?schema_address_addressCountry. } \
                            OPTIONAL {?uri schema:address ?schema_address. ?schema_address schema:postalCode ?schema_address_postalCode. }}"

                var url = "http://opendata.caceres.es/recurso/turismo/establecimientos/BarCopas/"
                return customPromise(url, query);
            },

            restaurantes: function () {
                var query = "SELECT ?uri ?geo_long ?om_tenedores ?schema_url ?geo_lat ?schema_email ?schema_telephone ?rdfs_label ?om_capacidadPersonas ?om_categoriaRestaurante ?schema_address_streetAddress ?schema_address_addressLocality ?schema_address_addressCountry ?schema_address_postalCode  WHERE {  \
                            ?uri a om:Restaurante.  \
                            OPTIONAL  {?uri geo:long ?geo_long. } \
                            OPTIONAL  {?uri om:tenedores ?om_tenedores. } \
                            OPTIONAL  {?uri schema:url ?schema_url. } \
                            OPTIONAL  {?uri geo:lat ?geo_lat. } \
                            OPTIONAL  {?uri schema:email ?schema_email. } \
                            OPTIONAL  {?uri schema:telephone ?schema_telephone. } \
                            OPTIONAL  {?uri rdfs:label ?rdfs_label. } \
                            OPTIONAL  {?uri om:capacidadPersonas ?om_capacidadPersonas. } \
                            OPTIONAL  {?uri om:categoriaRestaurante ?om_categoriaRestaurante. } \
                            OPTIONAL {?uri schema:address ?schema_address. ?schema_address schema:streetAddress ?schema_address_streetAddress. } \
                            OPTIONAL {?uri schema:address ?schema_address. ?schema_address schema:addressLocality ?schema_address_addressLocality. } \
                            OPTIONAL {?uri schema:address ?schema_address. ?schema_address schema:addressCountry ?schema_address_addressCountry. } \
                            OPTIONAL {?uri schema:address ?schema_address. ?schema_address schema:postalCode ?schema_address_postalCode. }}"
                var url = ""
                return customPromise(url, query);
            }
        }
    }]);

var limpiaResultado = function (datos) {
    var result = {};
    // Define las cabeceras del resultado (para por ejemplo poner las cabeceras de una tabla)
    result.vars = datos.data.head.vars;
    // Recorre el vector de resultados (fila) y el de cabeceras (clave) y recrea el vector resultado solo
    // con lo que nos interesa
    result.results = [];
    for (fila in datos.data.results.bindings) {
        var row = {};
        for (clave in result.vars) {
            var nombre = result.vars[clave]
            var valor = ""
            if(datos.data.results.bindings[fila][result.vars[clave]] != undefined)
                valor = datos.data.results.bindings[fila][result.vars[clave]].value
            row[nombre] = valor
        }
        result.results.push(row)
    }
    return result
};

/*
 Ejemplo de uso de este factory:

    SPARQLService.prueba()
        .then(function (datos) {
            console.log(datos)
        }).catch(function (err) {
    })

    Enlaces de prueba2:
    http://opendata.caceres.es/dataset/puntos-recogida-caceres
    http://opendata.caceres.es/sparql/?default-graph-uri=http%3A%2F%2Fopendata.caceres.es%2Frecurso%2Fmedio-ambiente%2Fresiduos-urbanos%2FPuntoRecogida%2F&query=select+%3Frdfs_label+%3Fom_numeroContenedores%0D%0Awhere%7B%0D%0A%3Furi+a+om%3APuntoRecogida.%0D%0A%3Furi+rdfs%3Alabel+%3Frdfs_label.%0D%0A%3Furi+om%3AnumeroContenedores+%3Fom_numeroContenedores.%0D%0A%7D&format=application%2Fsparql-results%2Bjson&timeout=0&debug=on
    http://opendata.caceres.es/sparql/


 */