# Instalacion

##### Python 3.5.2

https://www.python.org/downloads/

### Al instalar, que el check de "adjust the system PATH environment" esté activo

Comprobar si se puede ejecutar por consola:

```
python 
```

Si da error, probar:

```
python3
```
o
```
python3.5
```

Si no va ninguno, mirar si hace falta modificar el path, añadiendo:

```
C:\python35
```

Debemos de recordar cómo hemos accedido a él, para usarlo más adelante donde ponga "python"

## Dependencias

Antes de ejecutar el servidor, tenemos que instalar sus dependecias, para ello iremos hasta la carpeta del proyecto y ejecutaremos:

```
python -m pip install -r requirements.txt
```

# Ejecución

Para ejecutar el servidor:

```
python server.py
```
