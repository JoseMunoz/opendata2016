# Uso de SPARQLService

#### Composicion interna:

Para cada consulta  debemos de copiar-pegar la misma estructura que con las otras consultas, pero cambiando en el **config.data**:
 - "defalt-graph-uri": URL del grafo, viene indicado en la web
 - "query": Contendrá la query respectiva
 
```js
    prueba2: function () {
        const promise = new Promise(function (resolve, reject) {
            config.data = {
                "default-graph-uri": "http://opendata.caceres.es/recurso/medio-ambiente/residuos-urbanos/PuntoRecogida/",
                query: "select ?rdfs_label ?om_numeroContenedores " +
                "where{ " +
                "?uri a om:PuntoRecogida. " +
                "?uri rdfs:label ?rdfs_label. " +
                "?uri om:numeroContenedores ?om_numeroContenedores. " +
                "}",
                format: "application/sparql-results+json",
                timeout: 0,
                debug: "on"
            };
            $http(config).then(function (datos) {
                resolve(limpiaResultado(datos))
            }, function (err) {
                reject(new Error('No existe el dato'))
            });
        });
        return promise
    }
```

#### Ejemplo de uso de este factory:

````js
    SPARQLService.prueba()
        .then(function (datos) {
            console.log(datos)
        }).catch(function (err) {
    })
````

#### Formato devolucion:

Se realiza una limpieza de los datos después de que vuelvan para dejarlos con esta estructura:

```json
{
"vars": ["cabecera1", "cabecera2", ""],
"results": [
    {"cabecera1":"valor1",
     "cabecera2":"valor2",
     "...":"..."},
     
     {"cabecera1":"valor1",
     "cabecera2":"valor2",
     "...":"..."},
     
     "..."]
}
```

#### Enlaces de prueba2:

**Información en la página de OpenData:**

http://opendata.caceres.es/dataset/puntos-recogida-caceres

**URL para ver el resultado original de la consulta:**

http://opendata.caceres.es/sparql/?default-graph-uri=http%3A%2F%2Fopendata.caceres.es%2Frecurso%2Fmedio-ambiente%2Fresiduos-urbanos%2FPuntoRecogida%2F&query=select+%3Frdfs_label+%3Fom_numeroContenedores%0D%0Awhere%7B%0D%0A%3Furi+a+om%3APuntoRecogida.%0D%0A%3Furi+rdfs%3Alabel+%3Frdfs_label.%0D%0A%3Furi+om%3AnumeroContenedores+%3Fom_numeroContenedores.%0D%0A%7D&format=application%2Fsparql-results%2Bjson&timeout=0&debug=on

**URL para probar las consultas y ver el resultado en el mismo HTML (mejor para probar):**

http://opendata.caceres.es/consulta-sparql

**URL para probar las consultas y ver resultado en cualquier formato (para verlo en formato final marcar formato JSON):**

http://opendata.caceres.es/sparql/